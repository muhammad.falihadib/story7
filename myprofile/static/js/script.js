$( document ).ready( function() {
    $( "#accordion" ).accordion({
        header: "h2",
        collapsible: true,
        active: false,
    });
    $("#down1").click(function(){
        $(".line1").insertAfter(".line2");
    });
    $("#up1").click(function(){
        $(".line1").inserAfter(".line4");
    });
    $("#down2").click(function(){
        $(".line2").insertAfter(".line3");
    });
    $("#up2").click(function(){
        $(".line2").insertBefore(".line1");
    });
    $("#down3").click(function(){
        $(".line3").insertAfter(".line4");
    });
    $("#up3").click(function(){
        $(".line3").insertBefore(".line2");
    });
    $("#down4").click(function(){
        $(".line4").insertBefore(".line1");
    });
    $("#up4").click(function(){
        $(".line4").insertBefore(".line3");
    });
});